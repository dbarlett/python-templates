#!/usr/bin/env python

"""Use pyvirtualdisplay to run Selenium tests in Firefox on a headless system

"""

from pyvirtualdisplay import Display
from selenium import webdriver


def main():
    """
    Start an 1024x768 display, launch Firefox, and browse to test page.
    """
    try:
        display = Display(visible=0, size=(1024, 768))
        display.start()

        try:
            browser = webdriver.Firefox()
            browser.get("http://status.savanttools.com/?code=200%20OK")
            print browser.page_source
        finally:
            browser.close()
    finally:
        display.stop()

if __name__ == '__main__':
    main()
